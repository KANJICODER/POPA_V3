    ##  SC[ hkmf-mini] #########################################
    ## SEE[ hkmf-c11 ] for reference:  #########################
    ############################################################
    gcc                                                        \
        -x c                                                   \
        -c "PNG_ASSET_PACK_2022.C11"                           \
        -o  png_asset_pack_2022.obj                            \
                                                               \
            -Werror                                            \
            -Wfatal-errors                                     \
            -Wpedantic                                         \
            -Wall                                              \
            -Wextra                                            \
                                                               \
            -fstrict-aliasing                                  \
            -Wstrict-aliasing                                  \
                                                               \
            -Wfatal-errors                                     \
            -D WFATAL_ERRORS_STOPS_COMPILE_AT_FIRST_ERROR      \
                                                               \
            -std=c11                                           \
            -m64 ###############################################
 
    ##  STACK_OVERFLOW: stackoverflow.com/questions/2734719
    ##
    ##  ar: Archive Tool
    ##
    ##  rcs: 
    ##     : r : Insert with replacement
    ##     : c : Create New Archive
    ##     : s : write an index
    ##
    ar rcs PNG_ASSET_PACK_2022.a png_asset_pack_2022.obj
 
    read -p "[ENTER_TO_EXIT]:"      ####                    ####
    ############################################################

    ## #include "R:/AAC/PUTIN_OR_PUSSY_ASSETS/512_X_512/GEN/PNG_ASSET_PACK_2022.a"